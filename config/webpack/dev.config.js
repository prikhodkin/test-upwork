/* eslint-disable */
const path = require(`path`);
const paths = require(`../paths`);
const HtmlWebpackPlugin = require(`html-webpack-plugin`);
const {CleanWebpackPlugin} = require(`clean-webpack-plugin`);
const MiniCssExtractPlugin = require(`mini-css-extract-plugin`);
const CopyPlugin = require(`copy-webpack-plugin`);



const fs = require(`fs`);

function generateHtmlPlugins(templateDir) {
  const templateFiles = fs.readdirSync(path.resolve(`${paths.src}`, templateDir));
  return templateFiles.map((item) => {
    const parts = item.split(`.`);
    const name = parts[0];
    const extension = parts[1];
    return new HtmlWebpackPlugin({
      filename: `./${name}.html`,
      template: path.resolve(`${paths.src}`, `${templateDir}/${name}.${extension}`),
      inject: false,
    });
  });
}

const htmlPlugins = generateHtmlPlugins(`./pages`);

module.exports = {
  mode: `development`,
  devtool: `source-map`,
  entry: [
    `${paths.src}/js/index.js`,
    `${paths.src}/scss/main.scss`,
  ],
  output: {
    path: `${paths.build}`,
    filename: `./js/bundle.js`,
  },
  devServer: {
    contentBase: path.join(paths.src, `dist`),
    compress: true,
    port: 9000,
    writeToDisk: true,
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: [`raw-loader`]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [`babel-loader`],
      },
      {
        test: /\.(s*)css$/,
        use: [
          MiniCssExtractPlugin.loader,
          `css-loader?url=false`,
          `postcss-loader`,
          `sass-loader`,
        ],
      },

      {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: `file-loader`,
        options: {
          name: `fonts/[name].[ext]`
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        loader: `file-loader`,
        options: {
          name: `images/[name].[ext]`,
        },
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: `./css/main.css`,
    }),
    new CopyPlugin({
      patterns: [
        {from: `./src/fonts`, to: `./fonts`},
        {from: `./src/images`, to: `./images`},
        {from: `./src/vendor`, to: `./vendor`},
      ],
    }),
  ].concat(htmlPlugins),

};
