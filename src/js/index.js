import customSelect from 'custom-select';
import Chart from 'chart.js/auto';

const dashboardSelects = document.querySelectorAll(`.dashboard__select`);

dashboardSelects.forEach((el) => customSelect(el));


let ctx = document.getElementById(`myChart`);
let myChart = new Chart(ctx, {
  type: `line`,
  data: {
    labels: [`Jan`, `Fed`, `Mar`, `Apr`, `May`, `Jun`, `Jul`, `Aug`, `Sep`, `Oct`, `Nov`, `Dec`],
    datasets: [{
      label: ``,
      lineTension: 0.5,
      data: [200, 180, 250, 170, 200, 230, 170, 200, 200, 210, 200, 200, 500],
    }]
  },
  options: {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  }
});

